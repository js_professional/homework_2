const Ingredients = [
    'Булка',
    'Огурчик',
    'Котлетка',
    'Бекон',
    'Рыбная котлета',
    'Соус карри',
    'Кисло-сладкий соус',
    'Помидорка',
    'Маслины',
    'Острый перец',
    'Капуста',
    'Кунжут',
    'Сыр Чеддер',
    'Сыр Виолла',
    'Сыр Гауда',
    'Майонез'
];


class Burger {

    constructor() {
        this.composition = [];
        this.name = [];
        this.OurMenu = [];
    }

    add(name, ingredients, cookingTime) {
        this.composition.push(ingredients);
        this.name.push(name);

        this.OurMenu.push({
            name: name,
            composition: ingredients,
            cookingTime: cookingTime
        });

    }

    showComposition() {

        let {composition, name} = this;
        let compositionLength = composition.length;
        if (compositionLength !== 0) {
            composition.map((item, i) => {
                console.log('Состав бургера', name[i], item);
            });
        }

        console.log('-----------------------------');

        console.log('Наше меню:', this.OurMenu);
    }

}


class Order extends Burger {

    constructor(name, condition, value) {
        super();
        this.name = name;
        this.condition = condition;
        this.value = value;
        this.menu = BURGER.OurMenu;
        this.orders = [];
        this.id = 1;
    }


    addOrder() {
        this.orders.push({
            name: this.name,
            menu: this.menu
        });
    }


    include() {
        return this.menu.map(item => {
            return item.composition.includes(this.value);
        });
    }

    index() {
        if (this.condition === 'has') {
            return this.include().indexOf(true);
        }
        if (this.condition === 'except') {
            return this.include().indexOf(false);
        }
    };


    getOrder() {

        let checker = array => array.every(item => item === false);

        this.orders.map((item, i) => {
            let menu = item.menu.filter(b => b.name === item.name);

            if ( this.index() >= 0 && !checker(this.include()) ) {
                console.log(`Заказ: 1. Бургер ${item.menu[this.index()].name} с "${this.value}", будет готов через ${item.menu[this.index()].cookingTime} минут`);
            }

            if ( checker(this.include()) && this.condition  ) {
                let random = item.menu[Math.floor(Math.random() * item.menu.length)];
                console.log(`Заказ: 1. К сожалению, такого бургера у нас нет, можете попробовать "${random.name}", он будет готов через ${random.cookingTime} минут `);
            }

            if ( this.index() === undefined && checker(this.include()) ) {
                console.log(`Заказ: 1. Бургер ${item.name}, будет готов через ${menu[i].cookingTime} минут `);

            }
        });

    }

}


console.log("========================== Burgers  ============================================ ");


const BURGER = new Burger();

BURGER.add('Hamburger',
    ['Булка',
        'Огурчик',
        'Котлетка',
        'Капуста',
        'Кисло-сладкий соус',
        'Майонез'
    ],
    10);

BURGER.add('Cheeseburger',
    ['Булка',
        'Огурчик',
        'Котлетка',
        'Помидорка',
        'Капуста',
        'Сыр Чеддер',
        'Сыр Виолла',
        'Сыр Гауда',
        'Маслины',
    ],
    20);

BURGER.add('Double Cheeseburger',
    ['Булка',
        'Огурчик',
        'Котлетка',
        'Бекон',
        'Помидорка',
        'Капуста',
        'Сыр Чеддер',
        'Сыр Виолла',
        'Сыр Гауда',
        'Рыбная котлета',
        'Майонез'
    ],
    35);


BURGER.showComposition();


console.log("========================== Orders  ============================================= ");

const ORDER = new Order('Cheeseburger');
ORDER.addOrder();
ORDER.getOrder();

// const ORDER = new Order('', 'has', 'Кисло-сладкий соус');
// ORDER.addOrder();
// ORDER.getOrder();

// const ORDER = new Order('', 'except', 'Бикон');
// ORDER.addOrder();
// ORDER.getOrder();
















